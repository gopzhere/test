# Laravel-Booking
Laravel based application used to book rooms and bookings and manage bookings and clients.
For now this is a basic booking management system for hotels.

### Features
* Adding clients and modify them and rooms associated with clients.
* Adding a new Room or check the availability of the room.
* Create and manage bookings

## Installations / Instructions



 Open `.env` file to configure your `database` and it's `name`, `host`, and `password` 

``` composer install ```

``` cp .env.example .env ``` / ``` copy .env.example .env ```

``` php artisan key:generate ```

``` php artisan migrate --seed ```

``` php artisan serve ```

`localhost:8000`

## Login Credentials

user@hotel.com: 123456789
admin@hotel.com: 123456789


 







