@extends('layouts.master')


@section('content')
    <h2> View Bookings</h2>
    <hr>

    @include('errors.errors')
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>#Booking ID</th>
            <th>Client Name</th>
            <th>Room</th>
            <th>Floor</th>
            <th>Beds</th>
            <th>Type</th>
            <th>Booked At</th>
            <th>Booking End</th>
            <th>Booked By</th>
            <th>Status</th>
          
        </tr>
        </thead>
        <tbody>
        @foreach ($bookings as $booking)
            <tr>
                <td>{{ $booking->id }}</td>
                <td>{{ $booking->client->name }}</td>
                <td>{{ $booking->room->name }}</td>
                <td>{{ $booking->room->floor }}</td>
                <td>{{ $booking->room->beds }}</td>
                <td>{{ $booking->room->type }}</td>
                <td>{{ $booking->start_date }}</td>
                <td>{{ $booking->end_date }}</td>
                <td>{{ $booking->user->name }}</td>
                <td>
                    @if ($booking->status)
                        <label class="label label-primary text-xs">Booked</label>
                    @else
                        <label class="label label-warning text-xs">Canceled </label>
                    @endif
                </td>
              
            </tr>
        </tbody>
        @endforeach
    </table>
@endsection