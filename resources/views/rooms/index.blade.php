@extends('layouts.master')

@section('title')
    View Rooms
@endsection



@section('content')
    <h2>View Rooms</h2>
    <hr>
    @include('errors.errors')
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>#ID</th>
            <th>name</th>
            <th>Type</th>
            <th>Floor</th>
            <th>Beds</th>
            <th>Status</th>
           
        </tr>
        </thead>
        <tbody>
        @foreach ($rooms as $room)
            <tr>
                <td>{{ $room->id }}</td>
                <td>{{ $room->name }}</td>
                <td>{{ $room->type }}</td>
                <td>{{ $room->floor }}</td>
                <td>{{ $room->beds }}</td>
                <td>
                    @if ($room->status == 0)
                        <span class="label label-primary">Available</span>
                    @else
                        <span class="label label-danger">Not Available</span>
                    @endif
                </td>
                
            </tr>
        </tbody>
        @endforeach
    </table>
@endsection
