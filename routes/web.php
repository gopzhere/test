<?php

    // Dashboard
    Route::resource('/','DashboardController');
    Route::resource('userdashboard','UserDashboardController');

 
    // Rooms
    Route::resource('rooms','RoomController');

    // Bookings
    Route::resource('booking','BookingController');
    
    // Sessions
    Route::get('/login','SessionsController@create')->name('login');
    Route::post('/login','SessionsController@store');
    Route::get('/logout','SessionsController@destroy');

    // User
    Route::get('/user','UserController@index');